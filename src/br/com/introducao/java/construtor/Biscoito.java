package br.com.introducao.java.construtor;

public class Biscoito {
    private String nome;
    private int numero;

    Biscoito(){
        System.out.println("entrei no  construtor");
    }
    Biscoito(String nome){
        this.nome = nome;
    }
    Biscoito(int numero){
        this.numero = numero;
    }
    Biscoito(String nome, int numero){
        this.nome = nome;
        this.numero = numero;
    }
    public void setNome(String nome){
        this.nome = nome;
    }
    public String getNome(){
        return nome;
    }
    public int getNumero() {
        return numero;
    }
    public void setNumero(int numero) {
        this.numero = numero;
    }
}
