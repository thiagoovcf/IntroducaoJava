package br.com.introducao.java.construtor;

public class BiscoitoMain {
    public static void main(String[] args) {
       /* Biscoito biscoito = new Biscoito();
        System.out.println(biscoito.nome);
        System.out.println(biscoito.numero);
        biscoito.nome = "";
        biscoito.numero=17;

        Biscoito b2 = new Biscoito("Pedro");
        System.out.println(b2.nome);
        System.out.println(b2.numero);

        Biscoito b3 = new Biscoito(3);

        Biscoito b4 = new Biscoito("Nome", 17);
*/


        Biscoito b5 = new Biscoito("Maria", 30);
        System.out.println(b5.getNome());
        b5.setNome("Paula");
        System.out.println(b5.getNome());
        b5.setNome(b5.getNome().concat(" Freire"));
        System.out.println(b5.getNome());

//        System.out.println(b5.get);
    }
}
